package loader

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"

	"go-template-prj/docs"
)

func init() {
	docs.SwaggerInfo.Title = os.Getenv("APP_NAME")
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = os.Getenv("PUBLIC_URL")
}

// InitWebServer - creates gin.Engine instance
func InitWebServer(
	appCtx context.Context,
	engine *gin.Engine,
) (*http.Server, error) {
	engine.GET("/swagger/*any", ginSwagger.WrapHandler(
		swaggerFiles.Handler,
	))

	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", os.Getenv("APP_PORT")),
		Handler: engine,
	}

	server.BaseContext = func(net.Listener) context.Context {
		return appCtx
	}

	return server, nil
}
