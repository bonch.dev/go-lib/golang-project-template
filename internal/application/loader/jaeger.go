package loader

import (
	"context"
	"fmt"
	"os"

	"github.com/opentracing/opentracing-go"
	"github.com/sirupsen/logrus"
	"github.com/uber/jaeger-client-go/config"
	jtracerlogrus "gitlab.com/bonch.dev/go-lib/jtracer/logrus"
)

func InitJaeger(logger logrus.FieldLogger) (
	opentracing.Tracer,
	func(context.Context) error,
	error,
) {
	service := os.Getenv("APP_NAME")

	collectorEndpoint := os.Getenv("JAEGER_ENDPOINT")

	cfg := &config.Configuration{
		ServiceName: service,
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			CollectorEndpoint: fmt.Sprintf("%s/api/traces", collectorEndpoint),
		},
	}

	t, closer, err := cfg.NewTracer(
		jtracerlogrus.NewLogger(logger.WithField("Type", "Jaeger")).ToOption(),
	)
	if err != nil {
		return nil, nil, fmt.Errorf("ERROR: cannot init Jaeger: %e", err)
	}

	opentracing.SetGlobalTracer(t)

	return t, func(_ context.Context) error {
		return closer.Close()
	}, nil
}
