package loader

import (
	"context"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/bonch.dev/go-lib/jtracer"

	"go-template-prj/internal/runner"
)

func InitRunner(
	ctx context.Context,
	logger logrus.FieldLogger,
) (
	*runner.Runner,
	func(ctx context.Context) error,
	error,
) {
	runnerCtx, closeFunc := context.WithCancel(ctx)

	span := jtracer.FromContext(runnerCtx, jtracer.WithType("Runner"))
	defer span.Finish()

	leOpt := runner.Option(nil)
	if _, exists := os.LookupEnv("K8S_ELECTION"); exists {
		leOpt = runner.WithK8sLeaderElection(runnerCtx)
	}

	r := runner.New(
		runner.WithLogger(logger),
		runner.WithCronTask("*/1 * * * *", func() {
			// Some harmful jobs, that can be executed only by leader.
		}),
		leOpt,
	)

	return r, func(ctx context.Context) error {
		closeFunc()
		return nil
	}, nil
}
