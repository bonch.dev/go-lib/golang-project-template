package loader

import (
	"os"

	"github.com/sirupsen/logrus"

	"go-template-prj/internal/storage"
	s3storage "go-template-prj/internal/storage/s3"
)

func InitStorage(logger logrus.FieldLogger) storage.Storager {
	if s3Enabled() {
		bucket := os.Getenv("AWS_BUCKET")
		region := os.Getenv("AWS_DEFAULT_REGION")
		privateEndpoint := os.Getenv("AWS_ENDPOINT")
		publicEndpoint := os.Getenv("AWS_PUBLIC_ENDPOINT")

		return s3storage.NewStorage(logger, privateEndpoint, publicEndpoint, bucket, region)
	}

	return nil
}

func s3Enabled() bool {
	_, enabled := os.LookupEnv("AWS_BUCKET")
	return enabled
}
