package loader

import (
	"os"

	"github.com/evalphobia/logrus_sentry"
	"github.com/sirupsen/logrus"
	jtracerlogrus "gitlab.com/bonch.dev/go-lib/jtracer/logrus"
	"k8s.io/klog/v2"
)

func Logger() (*logrus.Logger, error) {
	level := os.Getenv("LOGGER_LEVEL")
	if level == "" {
		level = "debug"
	}

	logger := logrus.New()

	logLevel, err := logrus.ParseLevel(level)
	if err != nil {
		return nil, err
	}

	logger.SetLevel(logLevel)
	logger.SetFormatter(&logrus.TextFormatter{
		ForceColors: true,
	})

	InitLoggerSentryHook(logger)
	InitLoggerJaegerHook(logger)

	klog.ClearLogger()
	klog.SetOutputBySeverity("INFO", logger.WriterLevel(logrus.InfoLevel))
	klog.SetOutputBySeverity("WARNING", logger.WriterLevel(logrus.WarnLevel))
	klog.SetOutputBySeverity("ERROR", logger.WriterLevel(logrus.ErrorLevel))
	klog.SetOutputBySeverity("FATAL", logger.WriterLevel(logrus.FatalLevel))

	return logger, nil
}

func InitLoggerSentryHook(l *logrus.Logger) {
	dsn := os.Getenv("SENTRY_DSN")

	if dsn != "" {
		hook, err := logrus_sentry.NewAsyncSentryHook(dsn, []logrus.Level{
			logrus.PanicLevel,
			logrus.FatalLevel,
			logrus.ErrorLevel,
			logrus.InfoLevel,
			logrus.DebugLevel,
		})

		if err == nil {
			envMode, errEnv := os.LookupEnv("APP_ENV")
			if !errEnv {
				envMode = "unknown"
			}

			hook.SetEnvironment(envMode)

			release, founded := os.LookupEnv("CI_APPLICATION_TAG")
			if !founded {
				release = "memory-version"
			}

			hook.SetRelease(release)
			hook.StacktraceConfiguration.Enable = true
			hook.StacktraceConfiguration.Context = 3
			hook.StacktraceConfiguration.Skip = 6
			hook.StacktraceConfiguration.SendExceptionType = true
			hook.StacktraceConfiguration.Level = logrus.DebugLevel
			hook.StacktraceConfiguration.IncludeErrorBreadcrumb = true
			hook.StacktraceConfiguration.SwitchExceptionTypeAndMessage = true

			l.Hooks.Add(hook)
		}
	}
}

func InitLoggerJaegerHook(l *logrus.Logger) {
	h := jtracerlogrus.NewHook([]logrus.Level{
		logrus.PanicLevel,
		logrus.FatalLevel,
		logrus.ErrorLevel,
		logrus.InfoLevel,
		logrus.DebugLevel,
	})

	l.Hooks.Add(h)
}
