package loader

import (
	"context"
	"net/http"
	"sync"

	"github.com/sirupsen/logrus"
)

type routine struct {
	Routine  func() error
	Shutdown func(ctx context.Context) error
}

type Performer struct {
	logger   logrus.FieldLogger
	routines []routine
}

func NewPerformer(logger logrus.FieldLogger) *Performer {
	return &Performer{
		logger:   logger,
		routines: make([]routine, 0),
	}
}

func (s *Performer) Add(r func() error, shutdown func(ctx context.Context) error) {
	s.routines = append(s.routines, routine{
		Routine:  r,
		Shutdown: shutdown,
	})
}

func (s *Performer) Start() {
	for i := range s.routines {
		go func(i int) {
			if s.routines[i].Routine != nil {
				err := s.routines[i].Routine()
				s.logNestedError("start", err)
			}
		}(i)
	}
}

func (s *Performer) Shutdown(ctx context.Context) {
	wg := &sync.WaitGroup{}

	for i := range s.routines {
		wg.Add(1)

		sh := s.routines[i].Shutdown

		go func(f func(ctx context.Context) error) {
			if err := f(ctx); err != nil {
				s.logger.WithError(err).Error("Shutdown error")
			}

			wg.Done()
		}(sh)
	}

	wg.Wait()
}

func (s *Performer) logNestedError(t string, err error) {
	if err == nil {
		return
	}

	if err == http.ErrServerClosed {
		return
	}

	s.logger.WithError(err).Errorf("%s error", t)
}
