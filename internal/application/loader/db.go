package loader

import (
	"database/sql"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/volatiletech/sqlboiler/v4/boil"
)

func InitDB(logger *logrus.Entry) (*sql.DB, error) {
	db, err := sql.Open(
		os.Getenv("DATABASE_DRIVER"),
		os.Getenv("DATABASE_URL")+"?sslmode=disable",
	)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	if os.Getenv("DATABASE_DEBUG") == "true" {
		boil.DebugMode = true

		boil.DebugWriter = logger.WriterLevel(logrus.DebugLevel)
	}

	return db, nil
}
