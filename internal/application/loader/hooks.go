package loader

import (
	_ "go-template-prj/db/hooks" // initialize db hooks
)

func init() {
	// here you can place another hooks (like as Service Logic hooks, or etc...)
}
