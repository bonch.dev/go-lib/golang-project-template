//nolint:dirtyusage
package loader

import (
	"database/sql"

	"github.com/sirupsen/logrus"

	repositoryProvider "go-template-prj/internal/providers/repository"
	serviceProvider "go-template-prj/internal/providers/service"
)

func InitServiceProvider(
	logger logrus.FieldLogger,
) serviceProvider.Provider {
	p := serviceProvider.NewProviderConfiguration()

	return p
}

func InitRepositoryProvider(
	db *sql.DB,
) repositoryProvider.Provider {
	p := repositoryProvider.NewProviderConfiguration(db)

	return p
}
