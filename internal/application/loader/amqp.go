package loader

import (
	"context"
	"fmt"
	"os"

	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/bonch.dev/go-lib/retrier"

	observer "go-template-prj/internal/transport/ed/providers"
	rabbitmqNotifier "go-template-prj/internal/transport/ed/providers/rabbitmq"
	"go-template-prj/pkg/amqphelper"
)

type AMQPLoader struct {
	logger  logrus.FieldLogger
	tracer  opentracing.Tracer
	retrier *retrier.Retrier

	amqphelper.AMQPNetwork
}

func NewAMQPLoader(
	logger logrus.FieldLogger,
	tracer opentracing.Tracer,
) (*AMQPLoader, error) {
	settings := amqphelper.Settings{URL: EnvAMQPConnectionURL()}

	a := &AMQPLoader{
		logger:      logger.WithField("Type", "AMQPBinding"),
		tracer:      tracer,
		AMQPNetwork: amqphelper.AMQPNetwork{Settings: settings},
	}

	a.retrier = amqphelper.NewAMQPRetrier(a.logger, a.ReOpen)

	return a, nil
}

func (a *AMQPLoader) NewObserverProvider() *rabbitmqNotifier.ObserverProvider {
	return rabbitmqNotifier.NewObserverProvider(
		a.logger.WithField("Type", "ObserverProvider"),
		a.tracer,
		a.Settings,
	)
}

func (a *AMQPLoader) NewNotifierProvider() *rabbitmqNotifier.NotifierProvider {
	return rabbitmqNotifier.NewNotifierProvider(
		a.logger.WithField("Type", "NotifierProvider"),
		a.tracer,
		a.Settings,
	)
}

func (a *AMQPLoader) AddShutdown(op observer.ObserverProvider) func(ctx context.Context) error {
	return func(ctx context.Context) error {
		if op != nil {
			if err := op.Shutdown(ctx); err != nil {
				return err
			}
		}

		channel, err := a.Connection.Channel()
		if err != nil {
			return err
		}

		if err := channel.Close(); err != nil {
			return err
		}

		if err := a.Connection.Close(); err != nil {
			return err
		}

		return nil
	}
}

func (a *AMQPLoader) RegisterBindings() error {
	if err := a.registerDLXExchange(); err != nil {
		return fmt.Errorf(
			"failed to register dead letter exchange: %w",
			err,
		)
	}

	bindings := rabbitmqNotifier.Bindings()
	for i := range bindings {
		binding := &bindings[i]

		if err := a.registerExchange(binding.Exchange); err != nil {
			return fmt.Errorf(
				"failed to register exchange (%s): %w",
				binding.Exchange.String(),
				err,
			)
		}

		for queue := range binding.ObserversMap {
			if err := a.registerQueueWithDLX(queue); err != nil {
				return fmt.Errorf(
					"failed to register queue (%s) with dead letter exchange binding: %w",
					queue.Title,
					err,
				)
			}

			deadLetterQueueName := fmt.Sprintf("%s_DL", queue.Title)
			if err := a.registerQueue(deadLetterQueueName); err != nil {
				return fmt.Errorf(
					"failed to register dead letter queue (%s): %w",
					deadLetterQueueName,
					err,
				)
			}

			if err := a.registerBinding(binding.Exchange, queue); err != nil {
				return fmt.Errorf(
					"failed to register queue (%s) binding for exchange (%s): %w",
					queue.Title,
					binding.Exchange.String(),
					err,
				)
			}

			if err := a.registerBindingForDLX(queue); err != nil {
				return fmt.Errorf(
					"failed to register dead letter binding for queue (%s): %w",
					queue.Title,
					err,
				)
			}
		}
	}

	a.logger.Info("Registered all rabbitmq bindings")

	return nil
}

func (a *AMQPLoader) registerExchange(ex rabbitmqNotifier.Exchange) error {
	return a.retrier.Run(func() error {
		return errors.Wrapf(
			amqphelper.RequesterExchange(ex.String(), a.Channel),
			"Failed to register exchange %s", ex,
		)
	})
}

func (a *AMQPLoader) registerDLXExchange() error {
	return a.retrier.Run(func() error {
		return errors.Wrapf(
			amqphelper.RequesterDLXExchange(rabbitmqNotifier.DeadLetterExchange.String(), a.Channel),
			"Failed to register DLX exchange %s", rabbitmqNotifier.DeadLetterExchange,
		)
	})
}

func (a *AMQPLoader) registerQueue(queueName string) error {
	return a.retrier.Run(func() error {
		return errors.Wrapf(
			amqphelper.RequesterQueue(queueName, a.Channel, nil),
			"Failed to register queue %s", queueName,
		)
	})
}

func (a *AMQPLoader) registerQueueWithDLX(queue rabbitmqNotifier.Queue) error {
	return a.retrier.Run(func() error {
		return errors.Wrapf(
			amqphelper.RequesterQueue(queue.Title, a.Channel,
				amqp.Table{
					"x-dead-letter-exchange":    rabbitmqNotifier.DeadLetterExchange.String(),
					"x-dead-letter-routing-key": queue.Title,
				}),
			"Failed to register queue %s", queue.Title,
		)
	})
}

func (a *AMQPLoader) registerBinding(ex rabbitmqNotifier.Exchange, queue rabbitmqNotifier.Queue) error {
	return a.retrier.Run(func() error {
		return errors.Wrapf(
			amqphelper.RequesterBinding(
				ex.String(),
				queue.Title,
				"",
				a.Channel,
			),
			"Failed to register binding of %s queue to %s exchange", queue.Title, ex,
		)
	})
}

func (a *AMQPLoader) registerBindingForDLX(queue rabbitmqNotifier.Queue) error {
	return a.retrier.Run(func() error {
		return errors.Wrapf(
			amqphelper.RequesterBinding(
				rabbitmqNotifier.DeadLetterExchange.String(),
				fmt.Sprintf("%s_DL", queue.Title),
				queue.Title,
				a.Channel,
			),
			"Failed to register DLX binding of %s queue to %s exchange", queue.Title, rabbitmqNotifier.DeadLetterExchange,
		)
	})
}

func EnvAMQPConnectionURL() string {
	return fmt.Sprintf("amqp://%s:%s@%s:%s",
		os.Getenv("RABBITMQ_USER"),
		os.Getenv("RABBITMQ_PASSWORD"),
		os.Getenv("RABBITMQ_HOST"),
		os.Getenv("RABBITMQ_PORT"),
	)
}
