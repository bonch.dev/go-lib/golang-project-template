package loader

import (
	"os"

	"github.com/Unleash/unleash-client-go"
)

func InitUnleash() error {
	type metricsInterface struct{}

	unleashUrl, ok := os.LookupEnv("UNLEASH_URL")
	if !ok || unleashUrl == "" {
		return nil
	}

	appEnv := os.Getenv("APP_ENV")
	if appEnv == "" {
		appEnv = "production"
	}

	return unleash.Initialize(
		unleash.WithUrl(unleashUrl),
		unleash.WithInstanceId(os.Getenv("UNLEASH_INSTANCE_ID")),
		unleash.WithAppName(appEnv),
		unleash.WithListener(&metricsInterface{}),
	)
}
