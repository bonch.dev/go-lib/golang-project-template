package loader

import (
	"context"
	"os"
	"sync"

	"github.com/opentracing/opentracing-go"
	"github.com/sirupsen/logrus"

	"go-template-prj/internal/providers/repository"
	"go-template-prj/internal/providers/service"
	"go-template-prj/internal/transport/ed/events/base"
	edProviders "go-template-prj/internal/transport/ed/providers"
	edLocalProvider "go-template-prj/internal/transport/ed/providers/local"
)

// local (development) events chans for simulation ED transport behavior
var (
	localEventsChan          chan base.Event
	localEventsChanInitOnce  sync.Once
	localEventsChanCloseOnce sync.Once
)

func InitNotifiers(
	ctx context.Context,
	logger logrus.FieldLogger,
	tracer opentracing.Tracer,
) (
	closer func(ctx context.Context) error,
	err error,
) {
	var (
		np edProviders.NotifierProvider
	)

	switch {
	case AMQPEnabled():
		amqp, err := NewAMQPLoader(logger, tracer)
		if err != nil {
			return nil, err
		}

		np = amqp.NewNotifierProvider()

		closer = amqp.AddShutdown(nil)
	default:
		localEventsChanInitOnce.Do(func() {
			localEventsChan = make(chan base.Event)
		})

		np = edLocalProvider.NewNotifierProvider(
			logger.WithField("Type", "NotifierProvider"),
			tracer,
			localEventsChan,
		)

		closer = func(_ context.Context) error {
			localEventsChanCloseOnce.Do(func() {
				close(localEventsChan)
			})

			return nil
		}
	}

	edProviders.InitAndStartNotifier(ctx, np)

	return
}

func InitObservers(
	ctx context.Context,
	logger logrus.FieldLogger,
	tracer opentracing.Tracer,
	serviceProvider service.Provider,
	repositoryProvider repository.Provider,
) (
	closer func(ctx context.Context) error,
	err error,
) {
	var (
		op edProviders.ObserverProvider
	)

	switch {
	case AMQPEnabled():
		amqp, err := NewAMQPLoader(logger, tracer)
		if err != nil {
			return nil, err
		}

		op = amqp.NewObserverProvider()

		closer = amqp.AddShutdown(op)
	default:
		localEventsChanInitOnce.Do(func() {
			localEventsChan = make(chan base.Event)
		})

		op = edLocalProvider.NewObserverProvider(
			logger.WithField("Type", "ObserverProvider"),
			tracer,
			localEventsChan,
		)

		closer = func(_ context.Context) error {
			localEventsChanCloseOnce.Do(func() {
				close(localEventsChan)
			})

			return nil
		}
	}

	edProviders.InitAndStartObserver(ctx, op, serviceProvider, repositoryProvider)

	return
}

func AMQPEnabled() bool {
	val, enabled := os.LookupEnv("RABBITMQ_HOST")
	return enabled && val != ""
}
