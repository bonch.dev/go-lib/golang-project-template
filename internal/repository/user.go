package repository

import "go-template-prj/internal/providers/repository"

type UserRepository struct{}

func NewUserRepository() *UserRepository {
	return &UserRepository{}
}

var _ repository.UserStore = &UserRepository{}
