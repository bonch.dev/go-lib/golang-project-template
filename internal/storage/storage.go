package storage

import (
	"context"
	"io"
)

type Data struct {
	ContentType string
	URL         string `json:"url"`
	BinaryData  io.ReadSeeker
}

type Storager interface {
	Put(ctx context.Context, name string, data Data) (string, error)
	Get(ctx context.Context, name string) (Data, error)
	Delete(ctx context.Context, name string) error
}
