package s3

import (
	"github.com/sirupsen/logrus"
)

type logrusS3Logger struct {
	logger logrus.FieldLogger
}

func newLogrusS3Logger(logger logrus.FieldLogger) *logrusS3Logger {
	return &logrusS3Logger{logger: logger}
}

func (l logrusS3Logger) Log(i ...interface{}) {
	l.logger.Print(i...)
}
