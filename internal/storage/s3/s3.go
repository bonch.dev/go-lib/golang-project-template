package s3

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/private/protocol/rest"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/sirupsen/logrus"
	"gitlab.com/bonch.dev/go-lib/jtracer"

	"go-template-prj/internal/storage"
)

type Storage struct {
	session *session.Session
	svc     *s3.S3
	bucket  *string

	publicEndpoint *url.URL
	pathFormatter  func(fileName string) *string
}

func NewStorage(
	logger logrus.FieldLogger,
	privateEndpoint, publicEndpoint,
	bucket, region string,
) *Storage {
	sess := session.Must(session.NewSession())

	cfg := aws.NewConfig().
		WithCredentials(credentials.NewEnvCredentials()).
		WithS3ForcePathStyle(true).
		WithEndpoint(privateEndpoint).
		WithRegion(region).
		WithHTTPClient(http.DefaultClient).
		WithMaxRetries(aws.UseServiceDefaultRetries).
		WithLogger(newLogrusS3Logger(logger))

	svc := s3.New(sess, cfg)

	publicEndpointURL, _ := url.Parse(publicEndpoint)

	return &Storage{
		session: sess,
		svc:     svc,
		bucket:  &bucket,
		pathFormatter: func(fileName string) *string {
			var str string

			switch os.Getenv("APP_ENV") {
			case "production":
				str = fmt.Sprintf("/passport/%s", fileName)
			default:
				str = fmt.Sprintf("/passport/dev/%s", fileName)
			}

			return &str
		},
		publicEndpoint: publicEndpointURL,
	}
}

func (s *Storage) Put(ctx context.Context, name string, data storage.Data) (string, error) {
	span := jtracer.FromContext(ctx, jtracer.WithType("Storage"))
	defer span.Finish()

	_, err := s.svc.PutObjectWithContext(ctx, &s3.PutObjectInput{
		Body:        data.BinaryData,
		Bucket:      s.bucket,
		Key:         s.pathFormatter(name),
		ContentType: &data.ContentType,
	})
	if err != nil {
		return "", err
	}

	req, _ := s.svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket: s.bucket,
		Key:    s.pathFormatter(name),
	})

	rest.BuildAsGET(req)

	fileURL := req.HTTPRequest.URL

	if s.publicEndpoint != nil {
		fileURL.Host = s.publicEndpoint.Host
		fileURL.Scheme = s.publicEndpoint.Scheme
	}

	return fileURL.String(), nil
}

func (s *Storage) Get(ctx context.Context, name string) (storage.Data, error) {
	span := jtracer.FromContext(ctx, jtracer.WithType("Storage"))
	defer span.Finish()

	req, _ := s.svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket: s.bucket,
		Key:    s.pathFormatter(name),
	})

	rest.BuildAsGET(req)

	fileURL := req.HTTPRequest.URL

	if s.publicEndpoint != nil {
		fileURL.Host = s.publicEndpoint.Host
		fileURL.Scheme = s.publicEndpoint.Scheme
	}

	return storage.Data{
		URL:        fileURL.String(),
		BinaryData: nil,
	}, nil
}

func (s *Storage) Delete(ctx context.Context, name string) error {
	span := jtracer.FromContext(ctx, jtracer.WithType("Storage"))
	defer span.Finish()

	_, err := s.svc.DeleteObjectWithContext(ctx, &s3.DeleteObjectInput{
		Bucket: s.bucket,
		Key:    s.pathFormatter(name),
	})

	return err
}
