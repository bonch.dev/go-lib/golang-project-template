package middlewares

import (
	"database/sql"
	"fmt"
	"net/http"
	"net/url"
	"os"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/postgres"
	"github.com/gin-gonic/gin"

	"go-template-prj/internal/transport/http/routers"
)

const CookieMaxAge = 60 * 60 * 24 * 30

func GinSession(db *sql.DB) routers.RouterOption {
	postgresStore, err := postgres.NewStore(db, []byte(os.Getenv("SESSION_KEY")))
	if err != nil {
		panic(fmt.Sprintf("failed to create sessions store: %v", err))
	}

	publicURL, err := url.Parse(os.Getenv("PUBLIC_URL"))
	if err != nil {
		panic(fmt.Sprintf("failed to parse public url: %v", err))
	}

	postgresStore.Options(sessions.Options{
		Path:     "/",
		Domain:   publicURL.Host,
		MaxAge:   CookieMaxAge,
		Secure:   true,
		HttpOnly: true,
		SameSite: http.SameSiteNoneMode,
	})

	return func(e gin.IRouter) {
		e.Use(sessions.SessionsMany(
			[]string{},
			postgresStore,
		))
	}
}
