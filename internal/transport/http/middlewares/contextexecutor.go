package middlewares

import (
	"database/sql"

	"github.com/gin-gonic/gin"

	"go-template-prj/internal/repository/driver"
	"go-template-prj/internal/transport/http/routers"
)

func ContextExecutor(db *sql.DB) routers.RouterOption {
	return func(e gin.IRouter) {
		e.Use(func(ctx *gin.Context) {
			ctx.Set(driver.CtxExecutor.String(), db)
			ctx.Next()
		})
	}
}
