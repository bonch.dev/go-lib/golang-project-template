package routers

import (
	"go-template-prj/internal/providers/repository"
	"go-template-prj/internal/providers/service"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func ApiRoutes(
	logger logrus.FieldLogger,
	serviceProvider service.Provider,
	repositoryProvider repository.Provider,
) RouterOption {
	//baseController := controllers.NewBaseController(
	//	logger,
	//	serviceProvider,
	//	repositoryProvider,
	//)

	return func(e gin.IRouter) {
		extend(e.Group("api"), func(apiGroup gin.IRouter) {

		})
	}
}
