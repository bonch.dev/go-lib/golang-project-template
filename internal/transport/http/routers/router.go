package routers

import (
	"github.com/opentracing/opentracing-go"

	"gitlab.com/bonch.dev/go-lib/gin-sub/ginlogger"
	"gitlab.com/bonch.dev/go-lib/gin-sub/ginmetrics"
	"gitlab.com/bonch.dev/go-lib/gin-sub/gintracer"

	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type RouterOption func(e gin.IRouter)

func NewRouter(
	logger logrus.FieldLogger,
	tracer opentracing.Tracer,
	opts ...RouterOption,
) *gin.Engine {
	router := gin.New()
	router.Use(ginmetrics.Init(router))
	router.Use(ginlogger.Logger(logger))
	router.Use(gintracer.Tracer(tracer))

	pprof.Register(router, "__debug/pprof")

	for _, opt := range opts {
		opt(router)
	}

	return router
}

func extend(extended gin.IRouter, exFunc func(extended gin.IRouter)) {
	exFunc(extended)
}
