package routers

import (
	"go-template-prj/internal/providers/repository"
	"go-template-prj/internal/providers/service"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"

	"go-template-prj/internal/transport/http/controllers"
)

func DefaultRouter(
	logger logrus.FieldLogger,
	serviceProvider service.Provider,
	repositoryProvider repository.Provider,
) RouterOption {
	baseController := controllers.NewBaseController(
		logger,
		serviceProvider,
		repositoryProvider,
	)

	any := controllers.AnyController{Base: baseController}

	return func(e gin.IRouter) {
		if ee, ok := e.(*gin.Engine); ok {
			ee.NoRoute(any.Handle)
		}
	}
}
