package resources

import (
	"github.com/gofrs/uuid"
	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/types"

	"go-template-prj/pkg/nuuid"
)

type UserResource struct {
	ID         uuid.UUID      `json:"id"`
	CreatedAt  null.Time      `json:"created_at"`
	UpdatedAt  null.Time      `json:"updated_at"`
	DeletedAt  null.Time      `json:"deleted_at"`
	ClientID   nuuid.NullUUID `json:"client_id" swaggertype:"string"`
	ExternalID null.String    `json:"external_id" swaggertype:"string"`
	Name       string         `json:"name"`
	Balance    types.Decimal  `json:"balance"`
}

func UserElementTransform(user struct{}) *UserResource {
	var userResource UserResource

	return &userResource
}
