package request

import "github.com/gin-gonic/gin"

type Requester interface {
	Parse(ctx *gin.Context) error
}
