package user

import (
	"github.com/gin-gonic/gin"
	"github.com/volatiletech/sqlboiler/v4/types"

	"gitlab.com/bonch.dev/go-lib/jtracer"
)

type CreateRequest struct { //nolint:govet
	ID      string        `json:"id" example:"ext123"`
	Name    string        `json:"name" example:"Karl"`
	Balance types.Decimal `json:"balance" example:"300"`
}

func (r *CreateRequest) Parse(ctx *gin.Context) error {
	span := jtracer.FromGinContext(ctx, jtracer.WithType("Request"))
	defer span.Finish()

	return ctx.ShouldBindJSON(r)
}
