package user

import (
	"github.com/gin-gonic/gin"
	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/types"

	"gitlab.com/bonch.dev/go-lib/jtracer"
)

type UpdateRequest struct { //nolint:govet
	ID string `uri:"user" swaggerignore:"true"`

	Name    null.String       `json:"name,omitempty" example:"Karl" swaggertype:"string"`
	Balance types.NullDecimal `json:"balance,omitempty" example:"300" swaggertype:"number"`
}

func (r *UpdateRequest) Parse(ctx *gin.Context) error {
	span := jtracer.FromGinContext(ctx, jtracer.WithType("Request"))
	defer span.Finish()

	if err := ctx.ShouldBindUri(r); err != nil {
		return err
	}

	if err := ctx.ShouldBindJSON(r); err != nil {
		return err
	}

	return nil
}
