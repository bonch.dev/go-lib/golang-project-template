package controllers

import (
	"go-template-prj/internal/providers/repository"
	"go-template-prj/internal/providers/service"

	"github.com/sirupsen/logrus"
)

type Base interface {
	Logger() logrus.FieldLogger
	ServiceProvider() service.Provider
	RepositoryProvider() repository.Provider
}

type BaseController struct {
	logger             logrus.FieldLogger
	serviceProvider    service.Provider
	repositoryProvider repository.Provider
}

func NewBaseController(
	logger logrus.FieldLogger,
	serviceProvider service.Provider,
	repositoryProvider repository.Provider,
) *BaseController {
	return &BaseController{
		logger:             logger,
		serviceProvider:    serviceProvider,
		repositoryProvider: repositoryProvider,
	}
}

func (c *BaseController) Logger() logrus.FieldLogger {
	return c.logger
}

func (c *BaseController) ServiceProvider() service.Provider {
	return c.serviceProvider
}

func (c *BaseController) RepositoryProvider() repository.Provider {
	return c.repositoryProvider
}
