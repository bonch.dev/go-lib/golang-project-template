package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/bonch.dev/go-lib/jtracer"
)

type AnyController struct {
	Base
}

func (c *AnyController) Handle(ctx *gin.Context) {
	span := jtracer.FromGinContext(ctx, jtracer.WithType("Controller"))
	defer span.Finish()

	ctx.JSON(http.StatusOK, nil)
}
