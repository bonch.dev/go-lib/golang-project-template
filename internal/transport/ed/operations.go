package ed

import (
	"context"

	"go-template-prj/internal/transport/ed/events/base"
	"go-template-prj/internal/transport/ed/providers"
)

// Notify - Helper function, referenced for providers.Notify function
func Notify(ctx context.Context, event base.Event) {
	providers.Notify(ctx, event)
}
