package observers

import (
	"context"

	"go-template-prj/internal/providers/repository"
	"go-template-prj/internal/providers/service"
	"go-template-prj/internal/transport/ed/events/base"
)

type NotifyHandler func(ctx context.Context, event base.Event) error

type Observer interface {
	SetRepositoryProvider(repository.Provider)
	SetServiceProvider(service.Provider)

	RepositoryProvider() repository.Provider
	ServiceProvider() service.Provider

	Prepare() error
	GetType() string
	GetObservableEvents() []base.Event

	Handle(ctx context.Context, event base.Event) error
	ParseEvent(ctx context.Context, t base.EventType, data []byte) (base.Event, error)
}

type BaseObserver struct {
	repositoryProvider repository.Provider
	serviceProvider    service.Provider
}

func (o *BaseObserver) SetRepositoryProvider(p repository.Provider) {
	o.repositoryProvider = p
}

func (o *BaseObserver) SetServiceProvider(p service.Provider) {
	o.serviceProvider = p
}

func (o *BaseObserver) RepositoryProvider() repository.Provider {
	return o.repositoryProvider
}

func (o *BaseObserver) ServiceProvider() service.Provider {
	return o.serviceProvider
}

var Base = &BaseObserver{}
