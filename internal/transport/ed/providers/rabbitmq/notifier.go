package rabbitmq

import (
	"context"
	"encoding/json"

	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/bonch.dev/go-lib/jtracer"
	"gitlab.com/bonch.dev/go-lib/retrier"

	"go-template-prj/internal/transport/ed/events/base"
	"go-template-prj/pkg/amqphelper"
)

const (
	messageRetryCount = 1
	messageRetryLimit = 10
	messageDelay      = 0
)

var (
	ErrFailedGetExchange = errors.New("failed to get exchange for queue")
)

type NotifierProvider struct {
	amqphelper.AMQPNetwork

	tracer  opentracing.Tracer
	logger  logrus.FieldLogger
	retrier *retrier.Retrier
}

func NewNotifierProvider(
	l logrus.FieldLogger,
	t opentracing.Tracer,
	s amqphelper.Settings,
) *NotifierProvider {
	p := &NotifierProvider{
		logger: l,
		tracer: t,
	}

	p.Settings = s

	p.StartReconnector(p.logger)

	p.retrier = amqphelper.NewAMQPRetrier(p.logger, p.ReOpen)

	return p
}

func (p *NotifierProvider) Notify(ctx context.Context, event base.Event) {
	span := jtracer.FromContext(ctx, jtracer.WithType("Notifier"))
	defer span.Finish()

	if err := span.Tracer().Inject(span.Context(), opentracing.TextMap, event.Carrier()); err != nil {
		p.logger.
			WithError(err).
			WithContext(jtracer.ToContext(ctx, span)).
			Error("Notification error")

		return
	}

	body, err := json.Marshal(event)
	if err != nil {
		p.logger.WithError(err).Error("Notification error")

		return
	}

	exchange, err := p.findExchangeForEvent(event.GetType())
	if err != nil {
		p.logger.WithError(err).Error("Unknown event error")
	}

	for i := range bindings {
		binding := &bindings[i]

		if binding.Exchange == exchange {
			if err := p.sendEvent(exchange.String(), body); err != nil {
				p.logger.WithError(err).Error("Notification error")
			}
		}
	}
}

func (p *NotifierProvider) Start(context.Context) {
	err := p.OpenConnection()
	if err != nil {
		p.logger.WithError(err).Panic("Failed to start initial rabbitmq connection for NotificationProvider")
	}

	err = p.OpenChannel()
	if err != nil {
		p.logger.WithError(err).Panic("Failed to start initial rabbitmq channel for NotificationProvider")
	}

	p.logger.Info("Rabbitmq notifiers inited and started")
}

func (p *NotifierProvider) sendEvent(exchangeName string, body []byte) error {
	publishing := amqphelper.MakePublishing(
		body,
		exchangeName,
		messageRetryCount,
		messageRetryLimit,
		messageDelay,
	)

	return p.publish(exchangeName, &publishing)
}

// publish sends message to the specific queue
func (p *NotifierProvider) publish(exchangeName string, publishing *amqp.Publishing) error {
	return p.retrier.Run(func() error {
		return p.Channel.Publish(
			exchangeName,
			"",
			false,
			false,
			*publishing,
		)
	})
}

func (p *NotifierProvider) findExchangeForEvent(eventType base.EventType) (Exchange, error) {
	for i := range bindings {
		binding := &bindings[i]

		if binding.EventType == eventType {
			return binding.Exchange, nil
		}
	}

	return "", ErrFailedGetExchange
}
