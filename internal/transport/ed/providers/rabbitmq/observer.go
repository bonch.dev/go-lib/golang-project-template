package rabbitmq

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/bonch.dev/go-lib/jtracer"
	"gitlab.com/bonch.dev/go-lib/retrier"

	"go-template-prj/internal/transport/ed/events/base"
	"go-template-prj/internal/transport/ed/observers"
	"go-template-prj/pkg/amqphelper"
)

const (
	ackMaxCount = 5
)

type ObserverProvider struct {
	amqphelper.AMQPNetwork

	logger  logrus.FieldLogger
	tracer  opentracing.Tracer
	retrier *retrier.Retrier

	shutdown context.CancelFunc
	ctx      context.Context
}

func NewObserverProvider(
	l logrus.FieldLogger,
	t opentracing.Tracer,
	s amqphelper.Settings,
) *ObserverProvider {
	p := &ObserverProvider{
		logger: l,
		tracer: t,
	}

	p.Settings = s

	p.StartReconnector(p.logger)

	p.retrier = amqphelper.NewAMQPRetrier(p.logger, p.ReOpen)

	return p
}

func (p *ObserverProvider) Start(ctx context.Context) {
	p.ctx, p.shutdown = context.WithCancel(ctx)

	err := p.OpenConnection()
	if err != nil {
		p.logger.WithError(err).Panic("Failed to start initial rabbitmq connection for ObserverProvider")
	}

	err = p.OpenChannel()
	if err != nil {
		p.logger.WithError(err).Panic("Failed to start initial rabbitmq channel for ObserverProvider")
	}

	for i := range bindings {
		binding := &bindings[i]

		for queue, obs := range binding.ObserversMap {
			go func(et base.EventType, qu Queue, obs observers.Observer) {
				_ = p.retrier.
					RunCtx(p.ctx, func(ctx context.Context) error {
						return p.listenEvent(ctx, qu, et, obs)
					})
			}(binding.EventType, queue, obs)
		}
	}

	p.logger.Info("Rabbitmq observers inited and started")
}

func (p *ObserverProvider) Shutdown(_ context.Context) error {
	p.shutdown()

	return nil
}

func (p *ObserverProvider) listenEvent(
	ctx context.Context,
	queue Queue,
	et base.EventType,
	obs observers.Observer,
) (err error) {
	msgs, err := p.Channel.Consume(
		queue.Title,
		"",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		p.logger.Error(err)

		return err
	}

	defer func() {
		if r := recover(); r != nil {
			p.logger.
				WithField("QueueTitle", queue.Title).
				WithField("Error", r).
				Warn("Queue channel closed")

			err = errors.Errorf("RetryEvent closed due to %v", r)
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case d, ok := <-msgs:
			if !ok {
				p.logger.
					WithField("EventType", et).
					Error("Observer closed")

				return errors.New("rabbitmq messages channel is closed")
			}

			if err := p.workEvent(ctx, &d, obs, et); err != nil {
				p.Logger().WithError(err).
					WithField("EventType", et).
					WithField("Observer", obs.GetType()).
					Errorf("%s: Event (%s) notification failed", obs.GetType(), et)

				if nackErr := p.nack(&d); nackErr != nil {
					return errors.WithMessagef(nackErr, "nackErr when working event. workEventErr: %v", err)
				}

				continue
			}

			p.logger.Debugf("%s: Event (%s) notification success", obs.GetType(), et.String())

			if err := p.ack(&d); err != nil {
				p.logger.
					WithField("EventType", et).
					WithError(err).
					Error("Observer closed")

				return err
			}
		}
	}
}

func (p *ObserverProvider) extractSpanContext(body []byte) (opentracing.SpanContext, error) {
	tc := base.TracerCarrier{}
	tc.InitCarrier()

	if err := json.Unmarshal(body, &tc); err != nil {
		p.Logger().WithError(err).Warn("Tracer span extraction failed")

		return nil, errors.New("tracer span extraction failed")
	}

	sc, err := opentracing.GlobalTracer().Extract(opentracing.TextMap, tc.Carrier())
	if err != nil {
		p.Logger().WithError(err).Warn("Tracer span extraction failed")

		return nil, errors.New("tracer span extraction failed")
	}

	return sc, nil
}

func (p *ObserverProvider) workEvent(
	ctx context.Context,
	d *amqp.Delivery,
	o observers.Observer,
	et base.EventType,
) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = errors.Errorf("observer in panic: %v", r)
		}
	}()

	sc, err := p.extractSpanContext(d.Body)
	if err != nil {
		return err
	}

	span := jtracer.FromParentContext(jtracer.WithType("ObserverProvider"), sc)
	span.SetTag("Event", et.String())
	span.SetTag("Observer", o.GetType())

	defer span.Finish()

	event, err := o.ParseEvent(jtracer.ToContext(ctx, span), et, d.Body)
	if err != nil {
		return errors.Wrap(err, "parse event")
	}

	if event == nil {
		return errors.New("event is nil")
	}

	return errors.Wrap(
		o.Handle(jtracer.ToContext(ctx, span), event),
		"observer notify error",
	)
}

func (p *ObserverProvider) nack(d *amqp.Delivery) error {
	for reconnectTimes := 0; reconnectTimes < ackMaxCount; reconnectTimes++ {
		err := d.Nack(false, false)

		if err == amqp.ErrClosed {
			err = p.ReOpen()
			if err != nil {
				p.logger.WithError(err).Error("Failed to reopen rabbitmq channel in nack")
			}

			continue
		}

		return err
	}

	return errors.New("nack reconnectTimes exceeded")
}

func (p *ObserverProvider) ack(d *amqp.Delivery) error {
	for reconnectTimes := 0; reconnectTimes < ackMaxCount; reconnectTimes++ {
		err := d.Ack(false)

		if err == amqp.ErrClosed {
			err = p.ReOpen()
			if err != nil {
				p.logger.WithError(err).Error("Failed to reopen rabbitmq channel in ack")
			}

			continue
		}

		return err
	}

	return errors.New("ack reconnectTimes exceeded")
}

func (p *ObserverProvider) Logger() logrus.FieldLogger {
	return p.logger
}

func (p *ObserverProvider) RegisterObserver(o observers.Observer) error {
	if err := o.Prepare(); err != nil {
		return err
	}

	for _, event := range o.GetObservableEvents() {
		if err := p.addQueue(event, o); err != nil {
			return errors.Wrap(err, "failed to register queue for event")
		}
	}

	return nil
}

func (p *ObserverProvider) MustRegisterObserver(o observers.Observer) {
	if err := p.RegisterObserver(o); err != nil {
		p.logger.WithError(err).Panic("Failed to register observer")
	}
}

func (p *ObserverProvider) addQueue(event base.Event, obs observers.Observer) error {
	for i := range bindings {
		binding := &bindings[i]

		if event.GetType() == binding.EventType {
			qu := Queue{
				Title: fmt.Sprintf("%s_%s", event.GetType().String(), obs.GetType()),
			}

			binding.ObserversMap[qu] = obs

			return nil
		}
	}

	return amqphelper.ErrFailedGetExchange
}
