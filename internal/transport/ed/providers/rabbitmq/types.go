package rabbitmq

const (
	DeadLetterExchange Exchange = "DLX"
)

type (
	Exchange string
	Queue    struct {
		Title string
	}
)

func (ex Exchange) String() string {
	return string(ex)
}
