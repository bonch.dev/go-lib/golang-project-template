package rabbitmq

import (
	"go-template-prj/internal/transport/ed/events/base"
	"go-template-prj/internal/transport/ed/observers"
)

const (
// PersonSync  Exchange = "PersonSync"
)

type Binding struct {
	Exchange     Exchange
	EventType    base.EventType
	ObserversMap map[Queue]observers.Observer
}

func makeBinding(ex Exchange, event base.Event) Binding {
	return Binding{
		Exchange:     ex,
		EventType:    event.GetType(),
		ObserversMap: make(map[Queue]observers.Observer),
	}
}

var (
	bindings = []Binding{
		// makeBinding(PersonSync, &personEvents.SyncEvent{}),
	}
)

func AddBindings(bindings ...Binding) {
	bindings = append(bindings, bindings...)
}

func Bindings() []Binding {
	return bindings
}
