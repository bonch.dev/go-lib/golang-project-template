package providers

import (
	"context"
	"sync"

	"go-template-prj/internal/transport/ed/events/base"
)

type NotifierProvider interface {
	Notify(ctx context.Context, event base.Event)
	Start(ctx context.Context)
}

var (
	notifierProvider NotifierProvider

	onceNotifier sync.Once
)

func InitNotifier(p NotifierProvider) NotifierProvider {
	onceNotifier.Do(func() {
		notifierProvider = p
	})

	return notifierProvider
}

func InitAndStartNotifier(ctx context.Context, p NotifierProvider) NotifierProvider {
	p = InitNotifier(p)

	p.Start(ctx)

	return p
}

func Notify(ctx context.Context, event base.Event) {
	event.InitCarrier()

	notifierProvider.Notify(ctx, event)
}
