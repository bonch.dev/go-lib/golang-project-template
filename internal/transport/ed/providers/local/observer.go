package local

import (
	"context"
	"encoding/json"

	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/bonch.dev/go-lib/jtracer"

	"go-template-prj/internal/transport/ed/events/base"
	"go-template-prj/internal/transport/ed/observers"
)

type ObserverProvider struct {
	logger logrus.FieldLogger
	tracer opentracing.Tracer

	eventChan    <-chan base.Event
	observersMap map[base.EventType][]observers.Observer
}

func NewObserverProvider(
	l logrus.FieldLogger,
	t opentracing.Tracer,
	c <-chan base.Event,
) *ObserverProvider {
	return &ObserverProvider{
		logger:       l,
		tracer:       t,
		eventChan:    c,
		observersMap: make(map[base.EventType][]observers.Observer),
	}
}

func (p *ObserverProvider) RegisterObserver(o observers.Observer) error {
	if err := o.Prepare(); err != nil {
		return err
	}

	eList := o.GetObservableEvents()
	for i := range eList {
		t := eList[i].GetType()

		if _, ok := p.observersMap[t]; !ok {
			p.observersMap[t] = make([]observers.Observer, 0)
		}

		p.observersMap[t] = append(p.observersMap[t], o)
	}

	return nil
}

func (p *ObserverProvider) MustRegisterObserver(o observers.Observer) {
	if err := p.RegisterObserver(o); err != nil {
		p.logger.WithError(err).Panic("Failed to register observer")
	}
}

func (p *ObserverProvider) Start(ctx context.Context) {
	go func() {
		for e := range p.eventChan {
			sc, err := opentracing.GlobalTracer().Extract(opentracing.TextMap, e.Carrier())
			if err != nil {
				p.Logger().WithError(err).Warn("Tracer span extraction failed")

				continue
			}

			span := jtracer.FromParentContext(jtracer.WithType("Notifier"), sc)

			if e == nil {
				p.Logger().Warn("Local observer - Event is Nil")
				span.Finish()

				continue
			}

			obs := p.observersMap[e.GetType()]

			for i := range obs {
				o := obs[i]

				workingCtx := jtracer.ToContext(ctx, span)

				if err := p.workEvent(workingCtx, o, e); err != nil {
					p.logger.
						WithField("Observer", o.GetType()).
						WithField("Event", e.GetType()).
						WithError(err).
						WithContext(workingCtx).
						Error("observer work event failed")

					continue
				}

				p.logger.
					WithField("Observer", o.GetType()).
					WithField("Event", e.GetType()).
					WithContext(workingCtx).
					Debug("Event notified")
			}

			span.Finish()
		}
	}()
}

func (p *ObserverProvider) workEvent(ctx context.Context, o observers.Observer, e base.Event) error {
	span := jtracer.FromContext(ctx, jtracer.WithType("Notifier"))
	defer span.Finish()

	eventData, err := json.Marshal(e)
	if err != nil {
		return errors.Wrap(err, "local driver marshal error")
	}

	e, err = o.ParseEvent(jtracer.ToContext(ctx, span), e.GetType(), eventData) //nolint:govet
	if err != nil {
		return errors.Wrap(err, "observer parse error")
	}

	if e == nil {
		return errors.New("event is nil")
	}

	return errors.Wrap(
		o.Handle(jtracer.ToContext(ctx, span), e),
		"observer notify error",
	)
}

func (p *ObserverProvider) Logger() logrus.FieldLogger {
	return p.logger
}

func (p *ObserverProvider) Shutdown(_ context.Context) error {
	return nil
}
