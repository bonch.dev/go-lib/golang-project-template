package local

import (
	"context"

	"github.com/opentracing/opentracing-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/bonch.dev/go-lib/jtracer"

	"go-template-prj/internal/transport/ed/events/base"
)

type NotifierProvider struct {
	eventsChan chan base.Event
	logger     logrus.FieldLogger
	tracer     opentracing.Tracer
}

func NewNotifierProvider(
	l logrus.FieldLogger,
	t opentracing.Tracer,
	c chan base.Event,
) *NotifierProvider {
	return &NotifierProvider{
		eventsChan: c,
		tracer:     t,
		logger:     l,
	}
}

func (p *NotifierProvider) Notify(ctx context.Context, event base.Event) {
	go func() {
		span := jtracer.FromContext(ctx, jtracer.WithType("Notifier"))
		defer span.Finish()

		if err := span.Tracer().Inject(span.Context(), opentracing.TextMap, event.Carrier()); err != nil {
			p.logger.WithError(err).Error("Notification error")

			return
		}

		p.eventsChan <- event
	}()
}

func (p *NotifierProvider) Start(context.Context) {
	p.logger.Info("Local notifier inited and started")
}
