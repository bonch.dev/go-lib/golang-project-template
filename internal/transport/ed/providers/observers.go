package providers

import (
	"context"
	"sync"

	"github.com/sirupsen/logrus"

	"go-template-prj/internal/providers/repository"
	"go-template-prj/internal/providers/service"
	"go-template-prj/internal/transport/ed/observers"
)

type ObserverProvider interface {
	RegisterObserver(o observers.Observer) error
	MustRegisterObserver(o observers.Observer)
	Start(ctx context.Context)
	Logger() logrus.FieldLogger
	Shutdown(ctx context.Context) error
}

var (
	provider ObserverProvider

	once sync.Once
)

func InitObserver(
	p ObserverProvider,
	serviceProvider service.Provider,
	repositoryProvider repository.Provider,
) ObserverProvider {
	once.Do(func() {
		provider = p

		observers.Base.SetServiceProvider(serviceProvider)
		observers.Base.SetRepositoryProvider(repositoryProvider)
	})

	return provider
}

func InitAndStartObserver(
	ctx context.Context,
	p ObserverProvider,
	serviceProvider service.Provider,
	repositoryProvider repository.Provider,
) ObserverProvider {
	p = InitObserver(p, serviceProvider, repositoryProvider)

	p.Start(ctx)

	return p
}
