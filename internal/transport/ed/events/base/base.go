package base

import (
	"github.com/opentracing/opentracing-go"
)

type TracerCarrier struct {
	TM opentracing.TextMapCarrier `json:"tm"`
}

func (tc *TracerCarrier) Carrier() *opentracing.TextMapCarrier {
	return &tc.TM
}

func (tc *TracerCarrier) InitCarrier() {
	if tc.TM == nil {
		tc.TM = make(opentracing.TextMapCarrier)
	}
}

type Event interface {
	InitCarrier()
	Carrier() *opentracing.TextMapCarrier
	GetType() EventType
}

type EventType string

func (e EventType) String() string {
	return string(e)
}
