package runner

import (
	"time"

	"github.com/go-co-op/gocron"
	"github.com/sirupsen/logrus"
)

type Runner struct {
	logger      logrus.FieldLogger
	scheduler   *gocron.Scheduler
	runFunction func(r *Runner) error
}

func New(opts ...Option) *Runner {
	r := &Runner{
		logger:    logrus.New(),
		scheduler: gocron.NewScheduler(time.UTC),
		runFunction: func(r *Runner) error {
			r.scheduler.StartAsync()

			return nil
		},
	}

	for _, o := range opts {
		if o != nil {
			o(r)
		}
	}

	return r
}

func (r *Runner) Start() error {
	r.logger.Info("Runner started")

	return r.runFunction(r)
}
