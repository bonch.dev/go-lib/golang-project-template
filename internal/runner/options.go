package runner

import (
	"context"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	clientset "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/leaderelection"
)

type Option func(r *Runner)

func WithCronTask(cron string, task func()) Option {
	return func(r *Runner) {
		_, _ = r.scheduler.Cron(cron).Do(task)
	}
}

func WithLogger(logger logrus.FieldLogger) Option {
	return func(r *Runner) {
		r.logger = logger.WithField("Type", "Runner")
	}
}

func WithK8sLeaderElection(
	ctx context.Context,
) Option {
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}

	var (
		lockname  = os.Getenv("POD_LOCKNAME")
		podname   = os.Getenv("POD_NAME")
		namespace = os.Getenv("POD_NAMESPACE")
	)

	client := clientset.NewForConfigOrDie(config)
	lock := newLock(client, lockname, podname, namespace)

	return func(r *Runner) {
		r.runFunction = func(r *Runner) error {
			le, err := leaderelection.NewLeaderElector(leaderelection.LeaderElectionConfig{
				Lock:            lock,
				ReleaseOnCancel: true,
				LeaseDuration:   15 * time.Second,
				RenewDeadline:   10 * time.Second,
				RetryPeriod:     2 * time.Second,
				Callbacks: leaderelection.LeaderCallbacks{
					OnStartedLeading: func(ctx context.Context) {
						r.scheduler.StartAsync()
					},
					OnStoppedLeading: func() {
						r.scheduler.Stop()
					},
				},
			})
			if err != nil {
				return err
			}

			le.Run(ctx)

			return nil
		}
	}
}
