## Здесь хранятся DTO
DTO - это Data Transfer Objects. 
По опыту, в Golang лучше использовать DTO, которые реализуются интерфейсами

```go
type PersonDTO interface {
	ID() uuid.UUID
	Name() string
	Address() string
}
```