package service

/**
Store here your service contracts as Interface types.

Provide in your Services contract as
	var _ service.SomeService = &Service{}
to be sure, that your service is mockable and complete contract.
*/
