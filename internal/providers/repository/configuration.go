package repository

import (
	"database/sql"

	"github.com/friendsofgo/errors"
)

type ProviderConfiguration struct {
	db *sql.DB

	userRepository UserStore
}

func NewProviderConfiguration(db *sql.DB) *ProviderConfiguration {
	return &ProviderConfiguration{
		db: db,
	}
}

func (p *ProviderConfiguration) DB() *sql.DB {
	return p.db
}

func (p *ProviderConfiguration) SetUserRepository(userRepository UserStore) {
	p.userRepository = userRepository
}

func (p *ProviderConfiguration) UserRepository() (UserStore, error) {
	if p.userRepository == nil {
		return nil, errors.New("user repository not set")
	}

	return p.userRepository, nil
}
