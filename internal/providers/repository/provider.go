package repository

import (
	"database/sql"
)

type Provider interface {
	DB() *sql.DB

	UserRepository() (UserStore, error)
}
