############################
# STEP 1 build executable binary
############################
FROM golang:1.17-alpine AS builder

RUN echo -e "http://nl.alpinelinux.org/alpine/v3.15/main\nhttp://nl.alpinelinux.org/alpine/v3.15/community" \
            > /etc/apk/repositories

RUN apk add git ca-certificates

RUN mkdir /app
WORKDIR /app
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /go/bin/app ./cmd
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /go/bin/migration ./cmd/migration
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /go/bin/worker ./cmd/worker

############################
# STEP 2 run builded image
############################
FROM alpine

ARG CI_APPLICATION_TAG="no-tag"
ARG CI_PROJECT_NAME="no-name"

ENV CI_PROJECT_NAME ${CI_PROJECT_NAME}
ENV CI_APPLICATION_TAG ${CI_APPLICATION_TAG}

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

COPY --from=builder /go/bin/app         /go/bin/app
COPY --from=builder /go/bin/migration   /go/bin/migration
COPY --from=builder /go/bin/worker   	/go/bin/worker

COPY . /app

WORKDIR /go/bin
ENTRYPOINT ["/go/bin/app"]