LINTER_PLUGIN_DIRTYUSAGE_URL=https://gitlab.com/api/v4/projects/33176442/packages/generic/dirtyusage
LINTER_PLUGIN_DIRTYUSAGE_VERSION=0.1.9

COS := $(shell uname -s | tr '[:upper:]' '[:lower:]')
CARCH := $(shell uname -m | tr '[:upper:]' '[:lower:]')

configure_linter:
	curl -s --output ./pkg/dirtyusage.so $(LINTER_PLUGIN_DIRTYUSAGE_URL)/v$(LINTER_PLUGIN_DIRTYUSAGE_VERSION)/dirtyusage.v1.17.6.$(COS).$(CARCH).so
	chmod +x ./pkg/dirtyusage.so