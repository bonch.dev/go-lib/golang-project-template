package swagger

type DataMessageResponse struct {
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

type DataResponse struct {
	Data interface{} `json:"data"`
}

type ErrorResponse struct {
	Error   string `json:"error"`
	Message string `json:"message"`
}
