package errhttp

import (
	"context"
	"database/sql"
	"errors"
	"net/http"
	"strconv"
)

func ParseCode(err error) int {
	code, _ := Parse(err)

	return code
}

func Parse(err error) (code int, isError bool) {
	if err == nil {
		return http.StatusOK, false
	}

	switch {
	case handleServiceUnavailable(err):
		return http.StatusServiceUnavailable, true
	case handleBadRequest(err):
		return http.StatusBadRequest, true
	}

	return http.StatusConflict, true
}

func handleBadRequest(err error) bool {
	return errors.Is(err, strconv.ErrSyntax) ||
		errors.Is(err, strconv.ErrRange)
}

func handleServiceUnavailable(err error) bool {
	return errors.Is(err, sql.ErrConnDone) ||
		errors.Is(err, context.DeadlineExceeded) ||
		errors.Is(err, context.Canceled)
}
