package amqphelper

import (
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

type AMQPNetwork struct {
	Channel    *amqp.Channel
	Connection *amqp.Connection
	Settings   Settings

	mutex            sync.Mutex
	ChannelCloser    <-chan *amqp.Error
	ConnectionCloser <-chan *amqp.Error
}

func NewAMQPNetwork(settings Settings) AMQPNetwork {
	return AMQPNetwork{
		Settings: settings,
	}
}

func (a *AMQPNetwork) OpenConnection() error {
	var err error

	a.mutex.Lock()
	defer a.mutex.Unlock()

	a.Connection, err = amqp.Dial(a.Settings.URL)
	if err != nil {
		return err
	}

	a.ConnectionCloser = a.Connection.NotifyClose(make(chan *amqp.Error))

	return nil
}

func (a *AMQPNetwork) OpenChannel() error {
	var err error

	a.mutex.Lock()
	defer a.mutex.Unlock()

	a.Channel, err = a.Connection.Channel()
	if err != nil {
		return err
	}

	a.ChannelCloser = a.Channel.NotifyClose(make(chan *amqp.Error))

	return nil
}

func (a *AMQPNetwork) ReOpen() error {
	if a.Connection.IsClosed() {
		err := a.OpenConnection()
		if err != nil {
			return err
		}
	}

	err := a.OpenChannel()
	if err != nil {
		return err
	}

	return nil
}

func (a *AMQPNetwork) StartReconnector(logger logrus.FieldLogger) {
	go func() {
		for {
			select {
			case <-a.ChannelCloser:
				logger.Warn("RabbitMQ Channel closed, reopening")

				if err := a.ReOpen(); err != nil {
					logger.WithError(err).Panic("RabbitMQ Channel reopening failed. Aborting.")
				}
			case <-a.ConnectionCloser:
				logger.Warn("RabbitMQ Connection closed, reopening")

				if err := a.ReOpen(); err != nil {
					logger.WithError(err).Panic("RabbitMQ Connection reopening failed. Aborting.")
				}
			case <-time.After(time.Second):
			}
		}
	}()
}
