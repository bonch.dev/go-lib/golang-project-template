package amqphelper

import (
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/bonch.dev/go-lib/retrier"
)

var (
	ErrLimitReached      = errors.New("dropped queued message. retry limit reached")
	ErrFailedGetExchange = errors.New("failed to get exchange for queue")
)

func NewAMQPRetrier(logger logrus.FieldLogger, reOpenFunc func() error) *retrier.Retrier {
	return retrier.
		NewInfiniteRetrier().
		WithLogger(logger).
		WithBackoff(time.Second).
		WithClassifier(retrier.BlacklistClassifier{ErrFailedGetExchange}).
		WithErrorHandler(func(_, _ int, err error) {
			switch err {
			case amqp.ErrClosed:
				logger.WithError(err).Error("RabbitMQ connection lost. Retrying...")

				err = reOpenFunc()
				if err != nil {
					logger.WithError(err).Error("Failed to reconnect to RabbitMQ")
				}
			default:
				logger.WithError(err).Errorf("Started new retry. Failed to process func in retrier.")
			}
		})
}

// MakePublishing creates Publishing model with our data for sending messages to the server
func MakePublishing(body []byte, t string, retryCount, retryLimit, delay int32) amqp.Publishing {
	return amqp.Publishing{
		ContentType:  "text/plain",
		DeliveryMode: amqp.Persistent,
		Body:         body,
		Type:         t,
		Headers: map[string]interface{}{
			"x-retry":       retryCount, // counter of retries attempts
			"x-retry-limit": retryLimit, // limit number of retry attempts
			"x-delay":       delay,      // number of milliseconds the message should be delayed
		},
	}
}

func Reject(channel *amqp.Channel, d *amqp.Delivery, queueName string) error {
	err := d.Reject(false)
	if err != nil {
		return err
	}

	retries := d.Headers["x-retry"].(int32)
	retryLimit := d.Headers["x-retry-limit"].(int32)

	if retries < retryLimit {
		err := channel.Publish(
			"",
			queueName,
			false,
			false,
			MakePublishing(d.Body, d.Type, retries+1, retryLimit, 100), //nolint:gomnd
		)

		return err
	}

	return ErrLimitReached
}

type Settings struct {
	URL string
}

// RequesterQueue creates a queue if it doesn't already exist, or ensures that queue exists
func RequesterQueue(name string, channel *amqp.Channel, args amqp.Table) error {
	_, err := channel.QueueDeclare(
		name,
		true,
		false,
		false,
		false,
		args,
	)

	return err
}

func RequesterBinding(exchangeName, queueName, key string, channel *amqp.Channel) error {
	return channel.QueueBind(
		queueName,
		key,
		exchangeName,
		false,
		nil,
	)
}

// RequesterExchange creates an exchange if it doesn't already exist, or ensures that exchange exists
func RequesterExchange(name string, channel *amqp.Channel) error {
	return channel.ExchangeDeclare(
		name,
		"fanout",
		true,
		false,
		false,
		false,
		nil,
	)
}

func RequesterDLXExchange(name string, channel *amqp.Channel) error {
	return channel.ExchangeDeclare(
		name,
		"direct",
		true,
		false,
		false,
		false,
		nil,
	)
}
