package main

import (
	"context"
	"time"

	"go-template-prj/internal/application/loader"
	"go-template-prj/internal/transport/http/middlewares"
	"go-template-prj/internal/transport/http/routers"

	"github.com/sirupsen/logrus"
	_ "github.com/volatiletech/sqlboiler/v4/drivers/sqlboiler-psql/driver"
)

const (
	appShutdownDuration = 2 * time.Second
)

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	// load os.env settings
	if err := loader.LoadEnv(); err != nil {
		logrus.Warning(".env file not found")
	}

	appCtx, cancel := context.WithCancel(context.Background())
	defer cancel()

	logger, err := loader.Logger()
	if err != nil {
		logrus.WithError(err).Fatal("Logger initialization failed")
	}

	if err := loader.InitUnleash(); err != nil {
		logger.WithError(err).Fatal("Unleash initialization failed")
	}

	defer func() {
		if r := recover(); r != nil {
			logger.
				WithField("RecoverMessage", r).
				Fatal("Application Crashed")
		}
	}()

	performer := configure(appCtx, logger)

	performer.Start()

	<-loader.GracefulShutdown()
	logger.Info("Graceful Shutdown started...")

	ctx, forceCancel := context.WithTimeout(appCtx, appShutdownDuration)
	defer forceCancel()

	performer.Shutdown(ctx)
}

func configure(appCtx context.Context, logger *logrus.Logger) *loader.Performer {
	db, err := loader.InitDB(logger.WithField("Type", "Database"))
	if err != nil {
		logger.WithError(err).Fatal("DB initialization failed")
	}

	performer := loader.NewPerformer(logger)

	repositoryProvider := loader.InitRepositoryProvider(db)
	serviceProvider := loader.InitServiceProvider(logger)

	loader.InitStorage(logger)

	tracer, jaegerCloser, err := loader.InitJaeger(logger)
	if err != nil {
		logger.WithError(err).Fatal("Jaeger initialization Failed")
	}

	performer.Add(nil, jaegerCloser)

	// HTTP Driven Transport configuration
	{
		router := routers.NewRouter(
			logger,
			tracer,
			middlewares.ContextExecutor(db),
			// middlewares.GinSession(db), -- you can use Gin Sessions to store cookie information in session store
			routers.DefaultRouter(logger, serviceProvider, repositoryProvider),
			routers.ApiRoutes(logger, serviceProvider, repositoryProvider),
		)

		server, err := loader.InitWebServer(appCtx, router)
		if err != nil {
			logger.WithError(err).Fatal("WebServer initialization Failed")
		}
		performer.Add(server.ListenAndServe, server.Shutdown)
	}

	// Event Driven Transport configuration
	// Main (http web server) application needn't use Observers, 'cause it might be heavy to perform.
	{
		nedCloser, err := loader.InitNotifiers(appCtx, logger, tracer)
		if err != nil {
			logger.WithError(err).Fatal("ED notifiers initialization failed")
		}

		performer.Add(nil, nedCloser)
	}

	return performer
}
