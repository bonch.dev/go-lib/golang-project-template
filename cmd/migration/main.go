package main

import (
	"context"
	"database/sql"

	"go-template-prj/internal/transport/ed/providers"

	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"gitlab.com/bonch.dev/go-lib/migrator/grammar/postgres"
	"gitlab.com/bonch.dev/go-lib/migrator/migrator"

	_ "go-template-prj/db/migrations"
	"go-template-prj/internal/application/loader"
)

func main() {
	// load os.env settings
	if err := loader.LoadEnv(); err != nil {
		logrus.Warning(".env file not found")
	}

	logger, err := loader.Logger()
	if err != nil {
		logrus.WithError(err).Fatal("Logger initialization failed")
	}

	if err := loader.InitUnleash(); err != nil {
		logger.WithError(err).Fatal("Unleash initialization failed")
	}

	appCtx, cancel := context.WithCancel(context.Background())
	defer cancel()

	db, err := loader.InitDB(logger.WithField("Type", "Database"))
	if err != nil {
		logger.WithError(err).Fatal("DB initialization feailed")
	}

	migrateDB(appCtx, logger.WithField("Type", "DB Migration"), db)
	migrateED(appCtx, logger.WithField("Type", "ED Migration"), db)
}

func migrateDB(
	ctx context.Context,
	logger logrus.FieldLogger,
	db *sql.DB,
) {
	grammar := new(postgres.SQLGrammar)
	grammar.AddModifiers(postgres.DefaultModifiers()...)

	fieldLogger := logger.WithField("Type", "Migrator")

	m := migrator.Init(db, grammar, fieldLogger)

	logger.Info("DB Migration started")
	defer logger.Info("DB Migration ended")

	if err := m.MigrateCtx(ctx); err != nil {
		logger.WithError(err).Fatal("Migration")
	}
}

func migrateED(
	_ context.Context,
	logger logrus.FieldLogger,
	db *sql.DB,
) {
	if loader.AMQPEnabled() {
		logger.Info("ED Migration started")
		defer logger.Info("ED Migration ended")

		repositoryProvider := loader.InitRepositoryProvider(db)
		serviceProvider := loader.InitServiceProvider(logger)

		tracer, _, err := loader.InitJaeger(logger)
		if err != nil {
			logger.WithError(err).Panic("Failed to initialize Jaeger")
		}

		amqp, err := loader.NewAMQPLoader(logger, tracer)
		if err != nil {
			logger.WithError(err).Panic("Failed to process ED migrations")
		}

		if err := amqp.OpenConnection(); err != nil {
			logger.WithError(err).Panic("Failed to process ED migrations")
		}

		if err := amqp.OpenChannel(); err != nil {
			logger.WithError(err).Panic("Failed to process ED migrations")
		}

		op := amqp.NewObserverProvider()
		providers.InitObserver(op, serviceProvider, repositoryProvider)

		if err := amqp.RegisterBindings(); err != nil {
			logger.WithError(err).Panic("Failed to process ED migrations")
		}
	}
}
