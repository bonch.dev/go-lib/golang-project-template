package main

import (
	"context"
	"time"

	"go-template-prj/internal/application/loader"
	"go-template-prj/internal/transport/http/middlewares"
	"go-template-prj/internal/transport/http/routers"

	"github.com/sirupsen/logrus"
	_ "github.com/volatiletech/sqlboiler/v4/drivers/sqlboiler-psql/driver"
)

const (
	appShutdownDuration = 2 * time.Second
)

func main() {
	// load os.env settings
	if err := loader.LoadEnv(); err != nil {
		logrus.Warning(".env file not found")
	}

	appCtx, cancel := context.WithCancel(context.Background())
	defer cancel()

	logger, err := loader.Logger()
	if err != nil {
		logrus.WithError(err).Fatal("Logger initialization failed")
	}

	if err := loader.InitUnleash(); err != nil {
		logger.WithError(err).Fatal("Unleash initialization failed")
	}

	defer func() {
		if r := recover(); r != nil {
			logger.
				WithField("RecoverMessage", r).
				Fatal("Application Crashed")
		}
	}()

	performer := configure(appCtx, logger)

	performer.Start()

	<-loader.GracefulShutdown()
	logger.Info("Graceful Shutdown started...")

	ctx, forceCancel := context.WithTimeout(appCtx, appShutdownDuration)
	defer forceCancel()

	performer.Shutdown(ctx)
}

func configure(appCtx context.Context, logger *logrus.Logger) *loader.Performer {
	performer := loader.NewPerformer(logger)

	db, err := loader.InitDB(logger.WithField("Type", "Database"))
	if err != nil {
		logger.WithError(err).Fatal("DB initialization failed")
	}

	repositoryProvider := loader.InitRepositoryProvider(db)
	serviceProvider := loader.InitServiceProvider(logger)

	loader.InitStorage(logger)

	// Jaeger Tracing configuration
	tracer, jaegerCloser, err := loader.InitJaeger(logger)
	if err != nil {
		logger.WithError(err).Fatal("Jaeger initialization Failed")
	}

	performer.Add(nil, jaegerCloser)

	// HTTP Driven Transport configuration
	{
		router := routers.NewRouter(
			logger,
			tracer,
			middlewares.ContextExecutor(db),
			routers.DefaultRouter(logger, serviceProvider, repositoryProvider),
		)

		server, err := loader.InitWebServer(appCtx, router)
		if err != nil {
			logger.WithError(err).Fatal("WebServer initialization Failed")
		}

		performer.Add(server.ListenAndServe, server.Shutdown)
	}

	// Event Driven Transport configuration
	// Worker (Runner extended) application will run observers, as a part of heavy logics
	{
		nedCloser, err := loader.InitNotifiers(appCtx, logger, tracer)
		if err != nil {
			logger.WithError(err).Fatal("ED notifiers initialization failed")
		}

		performer.Add(nil, nedCloser)

		oedCloser, err := loader.InitObservers(appCtx, logger, tracer, serviceProvider, repositoryProvider)
		if err != nil {
			logger.WithError(err).Fatal("ED observers initialization failed")
		}

		performer.Add(nil, oedCloser)
	}

	// Internal Runner configuration.
	// Runner uses for crontab-like jobs or for other destructive operations
	// which MUST be executed once.
	{
		runner, runnerCloser, err := loader.InitRunner(appCtx, logger)
		if err != nil {
			logger.WithError(err).Fatal("Runner initialization failed")
		}

		performer.Add(runner.Start, runnerCloser)
	}

	return performer
}
