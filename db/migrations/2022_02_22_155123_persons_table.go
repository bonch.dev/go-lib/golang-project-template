package migrations

import (
	"gitlab.com/bonch.dev/go-lib/migrator/builder"
	"gitlab.com/bonch.dev/go-lib/migrator/migration"
	"gitlab.com/bonch.dev/go-lib/migrator/migrator"
)

func init() {
	migrator.AddMigrations(&M20220222155123PersonsTable{})
}

type M20220222155123PersonsTable struct {
	migration.BaseMigration
}

func (m M20220222155123PersonsTable) Up(b builder.Builder) {

}

func (m M20220222155123PersonsTable) Down(b builder.Builder) {

}

func (m M20220222155123PersonsTable) Name() string {
	return "M20220222155123PersonsTable"
}